/**
 * Created by psech on 2014-11-02.
 */
exports.config = {
    allScriptsTimeout: 11000,

    specs: [
        'e2e-tests/*e2e.js'
    ],

    multiCapabilities: [
        {
            'browserName': 'chrome',
            'chromeOptions': {
                'args': ['incognito']
            }
        }/*,
        // Commented as WebDriver supports only FireFox ESR in acceptable way
        {
            'browserName': 'firefox'
        }*/
    ],

    baseUrl: 'https://jira.atlassian.com/browse/TST',

    framework: 'jasmine',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    },

    onPrepare: function () {
        "use strict";
        require('jasmine-reporters');
        jasmine.getEnv().addReporter(
            new jasmine.JUnitXmlReporter('reports', true, true,
                'E2E-TestReport', true));
    }
};