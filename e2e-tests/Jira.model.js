/**
 * Created by psech on 2014-11-02.
 */
module.exports = (function () {
    "use strict";
    var model = {};

    // PRIVATE

    // PUBLIC
    model.USERNAME = 'sechu@poczta.onet.pl';
    model.PASSWORD = 'ASDqwe123';
    model.NEW_ISSUE_SUMMARY = 'My auto-generated issue ';
    model.SEARCH_ISSUE_KEY = 'TST-58308';

    /**
     * The button to invoke login overlay
     * @returns {ElementFinder}
     */
    model.loginButton = function () {
        return element(by.linkText('Log In'));
    };

    /**
     * The anchor (button) on the top action bar to create new issue
     * @returns {*}
     */
    model.createIssueAnchor = function () {
        return element(by.id('create_link'));
    };

    /**
     * The input to provide new/edit issue summary
     * @returns {*}
     */
    model.createIssueSummary = function () {
        return element(by.id('summary'));
    };

    /**
     * The input (button) to submit new issue
     * @returns {*}
     */
    model.createIssueSubmit = function () {
        return element(by.id('create-issue-submit'));
    };

    /**
     * The message PopUp with confirmation
     * @returns {*}
     */
    model.messageSuccessPopup = function () {
        return element(by.css('div[class^="aui-message success"]'));
    };

    /**
     * The quick search input on the top action bar
     * @returns {*}
     */
    model.quickSearchInput = function () {
        return element(by.id('quickSearchInput'));
    };

    /**
     * The summary of existing issue
     * @returns {*}
     */
    model.issueSummary = function () {
        return element(by.id('summary-val'));
    };

    /**
     * The list of all matching issues after search
     * @returns {*}
     */
    model.searchResults = function () {
        return element(by.css('div[class*="list-results-panel"]'));
    };

    /**
     * The single item on search result list that match provided key
     * @param issueKey The key or required issue
     * @returns {*}
     */
    model.searchResultItem = function (issueKey) {
        return element(by.css('li[data-key="' + issueKey + '"]'));
    };

    /**
     * The edit issue input (button)
     * @returns {*}
     */
    model.editIssueButton = function () {
        return element(by.id('edit-issue'));
    };

    /**
     * The input (button) to submit changes on updated issue
     * @returns {*}
     */
    model.editIssueSubmit = function () {
        return element(by.id('edit-issue-submit'));
    };

    return model;
})();