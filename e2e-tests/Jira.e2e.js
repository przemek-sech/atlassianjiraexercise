/**
 * Created by psech on 2014-11-02.
 */
describe('Jira', function () {
    "use strict";

    // This is for non AngularJS web pages
    browser.ignoreSynchronization = true;

    var model = require('./Jira.model'),
        service = require('./Jira.service');

    describe('new issue', function () {

        beforeEach(function () {
            browser.get('')
                .then(function () {
                    service.loginUser(model.USERNAME, model.PASSWORD);
                });
        });

        it('should be created with simple flow', function () {
            var timestamp = new Date().valueOf();
            model.createIssueAnchor().click()
                .then(function () {
                    return service.waitForElement(model.createIssueSummary());
                })
                .then(function () {
                    return model.createIssueSummary().sendKeys(model.NEW_ISSUE_SUMMARY, timestamp);
                })
                .then(function () {
                    return model.createIssueSubmit().click();
                })
                .then(function () {
                    return service.waitForElement(model.messageSuccessPopup());
                })
                .then(function () {
                    expect(browser.isElementPresent(model.messageSuccessPopup()))
                        .toBeTruthy();
                });
        });
    });

    describe('search', function () {

        beforeEach(function () {
            browser.get('')
                .then(function () {
                    service.loginUser(model.USERNAME, model.PASSWORD);
                });
        });

        it('should be performed by key', function () {
            model.quickSearchInput().sendKeys(model.SEARCH_ISSUE_KEY, protractor.Key.ENTER)
                .then(function () {
                    return service.waitForElement(model.issueSummary());
                })
                .then(function () {
                    expect(model.issueSummary().getText()).toBe('psech exercise Search test 1');
                });
        });

        it('should be performed by name', function () {
            model.quickSearchInput().sendKeys('psech exercise Search test', protractor.Key.ENTER)
                .then(function () {
                    return service.waitForElement(model.searchResults());
                })
                .then(function () {
                    expect(browser.isElementPresent(model.searchResultItem(model.SEARCH_ISSUE_KEY)))
                        .toBeTruthy();
                });
        });
    });

    describe('update issue', function () {

        beforeEach(function () {
            browser.get(browser.baseUrl + '-58310')
                .then(function () {
                    service.loginUser(model.USERNAME, model.PASSWORD);
                });
        });

        it('should be updated', function () {
            var timestamp = new Date().valueOf();
            model.editIssueButton().click()
                .then(function () {
                    return service.waitForElement(model.createIssueSummary());
                })
                .then(function () {
                    return model.createIssueSummary().sendKeys(
                        protractor.Key.ARROW_RIGHT,
                        protractor.Key.chord(protractor.Key.CONTROL, protractor.Key.BACK_SPACE),
                        timestamp);
                })
                .then(function () {
                    return model.editIssueSubmit().click();
                })
                .then(function () {
                    return browser.wait(function () {
                        return model.issueSummary().getText()
                            .then(function (summaryText) {
                                return protractor.promise.when(summaryText.indexOf(timestamp) > -1);
                            });
                    }, 10000);
                })
                .then(function () {
                    expect(model.issueSummary().getText()).toContain(timestamp);
                });
        });
    });
});