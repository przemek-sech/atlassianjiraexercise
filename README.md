#Atlassian JIRA Exercise#

##Purpose of the project##
The project was created to meet a recrutation process requirements:

Objective:

* Write a suite of three automated browser tests for JIRA, our issue tracking software.

Requirements:

* Use Selenium 2.0 (aka WebDriver): http://docs.seleniumhq.org/projects/webdriver/
* Tests must implement the page objects pattern: http://code.google.com/p/selenium/wiki/PageObjects
* You can write your tests in any language WebDriver supports
* The tests should verify that
    * New issues can be created
    * Existing issues can be updated
    * Existing issues can be found via JIRA’s search
* You can either download and install a trial instance of JIRA to test against or use the “A Test Project” project in  Atlassian’s public instance here: https://jira.atlassian.com/browse/TST
* If you do the former, please provide us with the information required to recreate a test environment to run 
* your tests against

##Technology stack##

* JavaScript
* [Protractor](http://angular.github.io/protractor/#/) - automation framework build on top of WebDriverJS. The main goal of tool is to automate AngularJS based application but it works also with regular web pages.
* [Jasmine](http://jasmine.github.io/1.3/introduction.html) - behavior-driven development framework for testing JavaScript code.

##Project structure##

```
C:\GIT\ATLASSIANJIRAEXERCISE
|   .gitignore
|   .jshintrc
|   package.json
|   protractor.conf.js		- test configuration file
|           
+---e2e-tests
|       Jira.e2e.js			- test/spec file
|       Jira.model.js		- DOM elements described as Pare Object Model
|       Jira.service.js		- common actions suproted by web page
|       
+---node_modules 
|   +---jasmine-reporters	- reporting tool, jUnit XML       
|   \---protractor          - automation tool
|       +---node_modules         
|       |   +---jasminewd			- adapter for Jasmine-to-WebDriverJS
|       |   +---saucelabs			- wrapper around the Sauce Labs REST API for [Node.js](http://nodejs.org/)  
|       |   \---selenium-webdriver	- Selenium 2 (WebDriver)    
|       |
|       \---selenium
|               chromedriver.exe
|               selenium-server-standalone-2.43.1.jar
|                   
\---reports					- catalog with reports
```

##Application Under Test##

The AUT is “A Test Project” project in Atlassian’s public instance.
Such approach allows ti take into account network delays, so the test should run in the same way everywhere.

##Run tests##

```bash
npm run protractor
```

The test project is build of [Node.js](http://nodejs.org/) modules so the above command will:

* `> npm install` - download and installation of necessary packages
* `> webdriver-manager update` - download and installation of selenium depandicties (including driver for chrome browser)
* `> protractor protractor.conf.js` - run selenium webdriver instance in server role and start tests

###Enviroment###

* Windows 7 x64
* Debian 7 x64
* Chrome v. 38.0.2125.111 m
* Firefox v. 31.2.0 ESR
* Protractor 1.3.1
* Selenium 2.43.1

###Expected result###

Console:

```
Starting selenium standalone server...
Selenium standalone server started at http://10.90.35.241:36064/wd/hub
....

Finished in 57.427 seconds
4 tests, 4 assertions, 0 failures

Shutting down selenium standalone server.
```

Xml:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<testsuites>
<testsuite name="Jira" errors="0" tests="0" failures="0" time="0" timestamp="2014-11-03T03:34:12">
</testsuite>
<testsuite name="Jira.new issue" errors="0" tests="1" failures="0" time="33.693" timestamp="2014-11-03T03:33:15">
  <testcase classname="Jira.new issue" name="should be created with simple flow" time="33.688"></testcase>
</testsuite>
<testsuite name="Jira.search" errors="0" tests="2" failures="0" time="17.888" timestamp="2014-11-03T03:33:48">
  <testcase classname="Jira.search" name="should be performed by key" time="8.232"></testcase>
  <testcase classname="Jira.search" name="should be performed by name" time="9.655"></testcase>
</testsuite>
<testsuite name="Jira.update issue" errors="0" tests="1" failures="0" time="5.837" timestamp="2014-11-03T03:34:06">
  <testcase classname="Jira.update issue" name="should be updated" time="5.837"></testcase>
</testsuite>
</testsuites>
```

##Test cases##

Following names are result of concatenation test suites and test cases names

* `Jira new issue should be created with simple flow` - create issue providing only summary
* `Jira search should be performed by key` - performs search in quick search providing issue key
* `Jira search should be performed by name` - performs search in quick search providing issue name (this is 4th test but could not resist ;) )
* `Jira update issue should be updated` - changes summary in issue edit mode

##Test approach and final notes##

The concept was to deliver project in the **Lean** methodology. Please consider this rather as an experiment then full blown project with all shiny features.
I used Protractor tool instead of pure WebDriver because I am more familiar with it and I hope you will find it interesting.

Test are running on both: chrome and firefox (and probably many more but I have not tested). The firefox is commented out as WebDriver supports ESR version

There was requirement to "write a suite of three automated browser tests". The test cases are described in the way that tempt to create one flow: create > search > update. From my point of view one flow is not the best solution because of the problems with maintaining and inconsistent results. I would be glad to discuss this in case of doubts.

I have noticed that public instance of Jira throws many warnings and errors to the console.
Warnings appear during every simple navigation, like:
```
Your are trying to create a ToggleBlock with selector '.toggle-trigger'.One already exists with this trigger so has been ignored. batch.js?locale=en-UK:2425
Resource interpreted as Font but transferred with MIME type font/x-woff: "https://jira.atlassian.com/s/en_UKvq70pt/64003/131/5.6.13/_/download/resources/com.atlassian.auiplugin:aui-experimental-iconfont/atlassian-icons.woff". 
```
Errors occur in non-deterministic way, after some time of inactivity. Might be connected with automatic test running on second instance of chrome.