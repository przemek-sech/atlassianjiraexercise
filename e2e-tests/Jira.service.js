/**
 * Created by psech on 2014-11-02.
 */
module.exports = (function () {
    "use strict";
    var service = {},
        model = require('./Jira.model');

    // PRIVATE

    // PUBLIC
    /**
     * Login user to Jira with provided username and password
     * @param username Username provide on login overlay
     * @param password Password to provide in login overlay
     * @returns {webdriver.promise.Promise|!webdriver.promise.Promise.<R>}
     */
    service.loginUser = function (username, password) {
        var promises = [];

        return browser.isElementPresent(model.loginButton())
            .then(function (userNotAuthorised) {
                if(userNotAuthorised) {
                    return model.loginButton().click()
                        .then(function () {
                            promises.push(element(by.id('username')).sendKeys(username));
                            promises.push(element(by.id('password')).sendKeys(password));
                            return protractor.promise.all(promises);
                        })
                        .then(function () {
                            return element(by.id('login-submit')).click();
                        });
                }
            });
    };

    /**
     * Waits for provided webElement
     * @param webElement The Element to wait for
     * @param timeout The timeout after which waiting fails
     * @returns {*|!webdriver.promise.Promise}
     */
    service.waitForElement = function (webElement, timeout) {
        timeout = typeof timeout === 'undefined' ? 10000 : timeout;

        return browser.wait(function () {
            return browser.isElementPresent(webElement);
        }, timeout);
    };

    return service;
})();